# promql-examples

### Show Overall CPU usage for a server: [Demo](https://prometheus.demo.cloudalchemy.org/graph?g0.range_input=1h&g0.expr=100%20*%20(1%20-%20avg%20by(instance)(irate(node_cpu_seconds_total%7Bmode%3D%27idle%27%7D%5B5m%5D)))&g0.tab=0)

> 100 * (1 - avg by(instance)(irate(node_cpu_seconds_total{mode='idle'}[5m])))
